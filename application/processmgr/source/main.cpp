#include <cstdio>
#include <unistd.h>
#include <signal.h>
#include <sys/wait.h>
#include "FmwLogger.hpp"
#include "processMgr.hpp"
#include <gtk/gtk.h>
/**
 *
 * @fn sig_handler
 * @brief end process
 * @param sig id (ctrl+c = 2)
 * @return none
 *
 */
void sig_handler(int signo)
{
    pid_t pid;
    int status;
    cout << "receive signo : " << signo << endl;
    if(processMgr::getInstance().getIsRunning()){
        while((pid = waitpid(-1, &status, WNOHANG))>0){
            cout << "stderr, SIGCHLD: CHILD EXIT : " << pid << endl;
            processMgr::getInstance().RemovedChildApp(pid);
        }
        return;
    }
}
#include <gtk/gtk.h>

void
hello (void)
{
  g_print ("Hello World\n");
}

void
destroy (void)
{
  gtk_main_quit ();
}

int main(int argc, char **argv)
{
    GMainLoop *pLoop = g_main_loop_new(NULL, FALSE);

    FmwLogger::getLogger().setAppName("processmgr");

    INFO_LOG("******************************** \n");
    INFO_LOG("Start processMgr\n");
    INFO_LOG("******************************** \n");

    signal(SIGCHLD, sig_handler);
    processMgr::getInstance().startApplication(pLoop);

    g_main_loop_run(pLoop);
}

