/**
 * @file faapplication.cpp
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2020-06-10
 *
 * @copyright Copyright (c) 2020
 *
 */

#include <iostream>
#include <signal.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <unistd.h>

#include "faapplication.hpp"
#include "faProcess.hpp"


using namespace std;
/**
 * @brief
 * @details
 * system() 함수로 호출하는 경우 새로운 shell 환경 내에서 실행되게 됨.
 * fork/exec 방식을 통해 부모 자식으로 엮이는 동시에 가벼운 프로세스 생성방식을 선택
 *
 */
bool CFaApplication::appFork() {
//#if 1
	pid_t childPid = fork();

	//DBG_LOG("childPid :%u\n", childPid);
	cout << "####### childPid : " << childPid << endl;

	// pid가 0인 경우는 child process이다.
	m_pid = childPid;
	if(m_pid == 0) {

		std::string path;
		char binPath[1024];

#ifdef DEF_FMW_DIR
		memset(binPath, 0, sizeof(binPath));
		strcpy(binPath, FMW_BIN_PATH);
#else
		if(getcwd(binPath, 1024) == NULL) {
			//ERROR_LOG("getcwd error!!\n");
			cout << "getcwd error!!" << endl;
			return false;
		}
#endif
		path.append(binPath).append("/");

		if(g_application_list.find(m_applicationId) != g_application_list.end())
		{
			path.append(g_application_list.at(m_applicationId));
		}

		//INFO_LOG("path = %s\n", path.c_str());
		cout << "path = " << path.c_str() << endl;

		const char* pname[]={path.c_str(),NULL};

		if(strcmp(g_application_list.at(m_applicationId).c_str(), g_application_list.at(ID_ALGORITHM).c_str()) == 0 ){
			if (execlp ("python3", "python3", path.c_str(), (char*)0) == -1)            {
				perror ("execlp");
				return false;
			}
		} else {
			if (execv (path.c_str(), (char**)pname) == -1)
			{
				perror ("execv");
				return false;
			}
		}
		//INFO_LOG("start [%s]\n", application_list.at(m_applicationId).c_str());
		cout << "start = [" << g_application_list.at(m_applicationId).c_str() << "]" << endl;
	}
	else {// parent process
		m_kickTime = steady_clock::now();
		if(m_pid < 0) { // fork fail
			return false;
		}
		setState(APP_STATE_READY);
	}
	return true;
}

/**
 * @brief 프로세스의 상태가 어떤지 모른다. watchdog kick을 보내지 못할 정도
 * 라면 SIGKILL 을 보내 종료시키자.
 * kill 하고 insertUnManaged()하면 상태변경하고 resetCount 증가시킨다.
 *
 */
void CFaApplication::appKill() {
	//INFO_LOG("kill [%s] PID : %d\n", application_list.at(m_applicationId).c_str(), m_pid);
	cout << "kill [" << g_application_list.at(m_applicationId).c_str() << "] PID : " << m_pid << endl;
	::kill(m_pid, SIGKILL);
}
