#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <ifaddrs.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#include "processMgr.hpp"


processMgr::~processMgr() {
    server_close(m_pServer);
    g_thread_join(mThreadserverThread);
	SAFE_DELETE(m_threadPool);
}

void processMgr::initializeServer(request_handler_t req_handler) {
    m_pServer = server_init(req_handler, SERVER_PORT);
    if(NULL != m_pServer) {
        mThreadserverThread = g_thread_new("serverThread", serverThread, this);
    }
}

gpointer processMgr::serverThread(gpointer user_data) {
    while (g_IsRunning) {
        server_accept_request(processMgr::getInstance().m_pServer);
    }
    return NULL;
}

void processMgr::request_handler(shared_ptr<ipc_msg_t> msg) {
    shared_ptr<ipc_msg_t>req = msg;
    processMgr::getInstance().handle_request(req);
}

void processMgr::handle_request(shared_ptr<ipc_msg_t> msg) {

    m_threadPool->EnqueueJob(receiveEvent, msg, this);
}

void processMgr::sendMessage(unsigned int to, char* buf, int len) {
    DBG_LOG("sendMessage : buf = %s, len = %d\n", buf, len);
    if(mClient.sockfd == 0) {
        if(client_init(&mClient, to, 0) == NULL) {
            ERROR_LOG("Error - client init!\n");
            return;
        }
    }

    if(client_send_request(&mClient, buf, len) != E_SUCCESS) {
        client_close(&mClient);
        if(client_init(&mClient, to, 0) == NULL) {
            ERROR_LOG("Error - client init == 2\n");
            return;
        }
        client_send_request(&mClient, buf, len);
    }
}

void processMgr::receiveEvent(shared_ptr<ipc_msg_t> msg, processMgr* pProcessMgr) {
    DBG_LOG("Receive data = %s\n", msg->data);
    string strData = msg->data;
    json::value jsonObject = json::value::parse(strData);
    string appid = jsonObject["appid"].as_string();
    string status  = jsonObject["status"].as_string();

    if(appid.compare(g_application_list.at(ID_CLOUD_MANAGER).c_str()) == 0 )
        {
        DBG_LOG("Launch QT app \n");
        // processMgr::getInstance().startChildApp(ID_UI_MANAGER, pProcessMgr);
       // sleep(1);
       // processMgr::getInstance().startChildApp(ID_ALGORITHM, pProcessMgr);
    }
}


void processMgr::startChildApp(unsigned int targetId, processMgr *pProcessMgr) {
    cout << "startChildApp : " << targetId << endl;
    scoped_lock lock(pProcessMgr->m_applicationListMutex);
    for(auto iter : pProcessMgr->m_applicationList) {
        unsigned int appId = targetId & iter.first;
        if(pProcessMgr->m_applicationList.find(appId) != pProcessMgr->m_applicationList.end()) {
            if(pProcessMgr->m_applicationList[appId]->getState() != APP_STATE_NONE  ||
                pProcessMgr->m_applicationList[appId]->getState() != APP_STATE_TERMINATED) {
                if(pProcessMgr->m_applicationList[appId]->appFork() == false) {
                    if(pProcessMgr->m_applicationList[appId]->getPid() == 0) {
                        cout << "exit() appFork fail : "<< g_application_list.at(pProcessMgr->m_applicationList[appId]->getApplicationId()).c_str() << endl;
                        //DBG_LOG("exit() appFork fail : %s\n", application_list.at(pProcessMgr->m_applicationList[appId]->getApplicationId()).c_str());
                        exit(-1);
                    } else if(pProcessMgr->m_applicationList[appId]->getPid() < 0) {
                       // pProcessMgr->m_watchdog.insertUnManagedMap(pProcessMgr->m_applicationList[appId]);
                    }
                }
                cout << "appFork : " << pProcessMgr->m_applicationList[appId]->getPid() << endl;
           }
        }
    }
}

void processMgr::RemovedChildApp(pid_t removedPid){
    cout << "RemovedChildApp" << endl;
    unsigned int targetId = ID_ALGORITHM | ID_UI_MANAGER;
    cout << "removedPid : " << removedPid<< endl;

    for(auto iter : this->m_applicationList) {
        unsigned int appId = targetId & iter.first;
        if(this->m_applicationList.find(appId) != this->m_applicationList.end()) {
            cout << "RemovedChildApp : appId = " << appId << endl;
        }
    }
}

// void processMgr::sendToQt(json::value value) {
//     string result = makeJsonForQT(value);
//     sendMessage(QT_PORT, (char*)result.c_str(), result.size());
// }



// string processMgr::makeJsonForQT(json::value srcJson) {
//     json::value dstJson;
//     unsigned char* decodeData = NULL;
//     int decodedSize = 0;
//     char resultPath[FILE_PATH_LEN];
//     string face, time, result;

//     face = srcJson["face"].as_string();
//     time = srcJson["time"].as_string();
//     dstJson["cameraNo"] = srcJson["cameraNo"];
//     dstJson["name"] = srcJson["name"];
//     dstJson["time"] = srcJson["time"];
//     DBG_LOG("face size = %d\n", face.size());

//     decodeData = (unsigned char*)malloc(face.size());
//     memset(decodeData, 0, face.size());
//     decodedSize = base64_decode(face.c_str(), decodeData, face.size());
//     DBG_LOG("decodedSize = %d\n", decodedSize);
//     sprintf(resultPath, "%s%s", IMAGE_DIR_PATH, time.c_str());
//     dstJson["face"] = json::value(resultPath);
//     processMgr::getInstance().writeBinData(resultPath, decodeData, decodedSize);
//     free(decodeData);
//     result = dstJson.serialize();

//     return result;
// }

