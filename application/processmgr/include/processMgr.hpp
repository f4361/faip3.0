/**
 * @file cloudMgr.hpp
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2022-04-01
 *
 * @copyright Copyright (c) 2022
 *
 */

#pragma once

#include <thread>
#include <mutex>
#include <map>
#include <list>
#include <iostream>
#include <cpprest/http_listener.h>
#include <cpprest/http_msg.h>
#include <cpprest/http_client.h>
#include <sys/socket.h>
#include "FmwAppCore.hpp"
#include "faProcess.hpp"
#include "faapplication.hpp"
#include "ipc.h"
#include "common.h"


using namespace std;
using namespace web;

class processMgr : public Singleton<processMgr>
{
    friend class Singleton<processMgr>;

private:


protected:
    processMgr() {
        for(auto iter : g_application_list) {
            m_applicationList.insert(make_pair(iter.first, make_shared<CFaApplication>(iter.first)));
	    }
    }
    virtual ~processMgr();

public:
    static void request_handler(shared_ptr<ipc_msg_t> msg);
    virtual void handle_request(shared_ptr<ipc_msg_t> msg);
    void initializeServer(request_handler_t req_handler);

    void startApplication(GMainLoop *pLoop) {
        m_threadPool = new FmwThread(20);
        m_pFmwAppCore.setGmainLoop(pLoop);
        processMgr::getInstance().initializeServer(request_handler);
        startChildApp(ID_CLOUD_MANAGER, this);
    }

    static gpointer serverThread(gpointer user_data);
    void sendMessage(unsigned int to, char* buf, int len);
    static void receiveEvent(shared_ptr<ipc_msg_t> msg, processMgr* pProcessMgr);

    void startChildApp(unsigned int targetId, processMgr *pProcessMgr);
    void RemovedChildApp(pid_t removedPid);
    int getIsRunning() {
        return g_IsRunning;
    }
public:
    int prcessmgr_fd;
    int mPort;

    GThread *mThreadserverThread;
    ipc_server_t *m_pServer;
    ipc_client_t mClient;
    map<unsigned int, shared_ptr<CFaApplication>> m_applicationList;
    std::mutex m_applicationListMutex;
};
