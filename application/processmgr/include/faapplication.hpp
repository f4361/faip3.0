/**
 * @file faapplication.hpp
 * @author your name (you@domain.com)
 * @brief 어플리케이션 정보를 담고 있는 클래스, 주 목적은 watchdog과 실행상태를 관리한다.
 * @version 0.1
 * @date 2020-06-10
 *
 * @copyright Copyright (c) 2020
 *
 */
#ifndef _FAAPPLICATION_HPP
#define _FAAPPLICATION_HPP
#include <memory>
#include <chrono>
using namespace std::chrono;

#define APP_ALIVE  "alive"

/**
 * @brief 앱의 실행 상태
 *
 */
enum {
    APP_STATE_NONE,
    APP_STATE_READY,
    APP_STATE_RUNNING,
    APP_STATE_TERMINATED,
};

class CFaApplication
{
public:
    CFaApplication(unsigned int applicationId)
    : m_applicationId(applicationId),
    m_pid(0),
    m_state(APP_STATE_NONE),
    m_ResetCount(0)
    {

    }

    /**
     * @brief Destroy the CFaApplication object
     * 소멸자 호출여부를 확인하기 위해 로그 출력 확인함.
     */
    ~CFaApplication() {

    }

    inline void setState(unsigned int state) { m_state = state; }
    inline unsigned int getState() { return m_state; }
    inline void clearResetCount() { m_ResetCount = 0; } /**< Noti_Ready를 받고 어플리케이션이 정상적으로 시작되었다고 판단되면 호출 */
    inline void increaseResetCount() { m_ResetCount++; }
    inline unsigned int getResetCount() { return m_ResetCount; }
    inline unsigned int getApplicationId() { return m_applicationId; }
    inline pid_t getPid() { return m_pid; }
    inline steady_clock::time_point& getKickTime() { return m_kickTime; }
    inline void updateKick() { m_kickTime = steady_clock::now(); }

    bool appFork();
    void appKill();


private:

    unsigned int    m_applicationId;
    pid_t           m_pid;
    unsigned int    m_state;
    unsigned int    m_ResetCount;    /**< 연속 2번 application reset 에 대한 문제 발생시 체크 */
    steady_clock::time_point m_kickTime;

};

#endif // _FAAPPLICATION_HPP
