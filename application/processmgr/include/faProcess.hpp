/**
 * @file faProcess.hpp
 * @author Kyunghoon Lim (kelvin.kh.lim@funzin.co.kr)
 * @brief
 * @version 0.1
 * @date 2021-03-03
 *
 * @copyright Copyright (c) 2021
 *
 */

#include <map>

using namespace std;

#ifndef _FAPROCESS_HPP
#define _FAPROCESS_HPP

#define ID_RUN_MANAGER          0x00000001
#define ID_CLOUD_MANAGER        0x00000002
#define ID_UI_MANAGER           0x00000004
#define ID_ALGORITHM            0x00000008
#define ID_FOTA_MANAGER         0x00000010

const map<unsigned int, std::string> g_application_list{
	{ID_RUN_MANAGER,"runmanager"},
	{ID_CLOUD_MANAGER, "cloudmgr"},
    {ID_UI_MANAGER,"FAIP_Qt"},
	{ID_ALGORITHM,"num_test.py"}
};

#endif	//_FAPROCESS_HPP