#ifndef _IPC_H_
#define _IPC_H_
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <memory>

#include "common.h"

#define SERVER_IP           "127.0.0.1"
#define FILE_NAME_LEN       1024
#define BACKLOG             10

#define SERVER_PORT                   4444

using namespace std;

typedef struct ipc_msg {
    char data[10*1024];
} ipc_msg_t;

typedef void (*request_handler_t) (shared_ptr<ipc_msg_t>  req);

typedef struct ipc_server {
    int sockfd;                         /* Socket fd of the server */
    int client_fd;
    pthread_t thread_id;
    request_handler_t request_handler;
} ipc_server_t;

typedef struct ipc_client {
    int sockfd;         /* Socket fd of the client */
} ipc_client_t;

ipc_server_t *server_init(request_handler_t req_handler, unsigned int port);
int server_accept_request(ipc_server_t *server);
ssize_t recv_data(int sockfd, char *buf, ssize_t len, int flags);
void server_close(ipc_server_t *server);
ipc_client_t *client_init(ipc_client_t *client, unsigned int port, int timeout);
int client_send_request(ipc_client_t *client, char *buf, int len);
void client_close(ipc_client_t *client);

#endif // _IPC_H_