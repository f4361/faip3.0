/**
 * @file FmwLogger.hpp
 * @author Kyunghoon Lim (kyunghoon.lim@funzin.co.kr)
 * @brief 
 * @version 0.1
 * @date 2021-03-03
 * @details 로그용 버퍼사이즈를 제한합니다. 로그 버퍼를 많이 사용하는 경우 메시지 전송에 오류가 발생합니다.
 * @copyright Copyright (c) 2021
 * 
 */

#ifndef _FMWLOGGER_HPP
#define _FMWLOGGER_HPP

#include <iostream>
#include <string>
#include <chrono>
#include <sstream>
#include <iomanip>
#include <ctime>
#include <map>
#include <fstream>
#include <dirent.h>

#include "common.h"

#define LOG_STR_MAX 1024

#define DBG_LOG(fmt, ...)   FmwLogger::getLogger().printLog("D",fmt, ##__VA_ARGS__);
#define INFO_LOG(fmt, ...)  FmwLogger::getLogger().printLog("I",fmt, ##__VA_ARGS__);
#define WARN_LOG(fmt, ...)  FmwLogger::getLogger().printLog("W",fmt, ##__VA_ARGS__);
#define ERROR_LOG(fmt, ...) FmwLogger::getLogger().printLog("E",fmt, ##__VA_ARGS__);
#define REMOVE_LOG_TIMER()  FmwLogger::getLogger().removeTimer();

using namespace std;

class FmwLogger
{
public:
    static FmwLogger& getLogger(){
        static FmwLogger logger;
        return logger;
    }

    void printLog(string level, const char * fmt, ...);
    void setAppName(string name);
    void setLogLevel(uint8_t level);
    uint8_t getLogLevel();    
    void removeTimer();

private:
    FmwLogger();
    ~FmwLogger();

    uint8_t m_appLoglevel;
    string m_appName;
    map <string, string> m_appNameMap;    
    string m_logFileName;
    string m_logDirPath;
    string m_logFullPath;
    guint  m_checkLogTimerId;


    void setLogFile();
    void checkLogFile();
    bool checkLogFileSize(string strName);
    bool renameLogFile();
    static int compare(const void *a, const void *b);
    static gboolean checkLogTimeoutCb(gpointer user_data);
};

#endif // _FMWLOGGER_HPP
