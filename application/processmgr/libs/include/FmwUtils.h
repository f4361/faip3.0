/**
 * @file FmwUtils.hpp
 * @author Kyunghoon Lim (kelvin.kh.lim@funzin.co.kr)
 * @brief 
 * @version 0.1
 * @date 2021-03-03
 * @details Null
 * @copyright Copyright (c) 2021
 * 
 */

#ifndef _FMWUTILS_H
#define _FMWUTILS_H

#include <iostream>
#include <cstdlib>
#include <cstdint>
#include <string>
#include <chrono>
#include <sstream>
#include <iomanip>
#include <ctime>
#include <map>
#include <vector>

#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include "common.h"

#define UTC_TIME 1
#define LOCAL_TIME 2
#define TIME_LEN          27


typedef enum {
    EXEC_NO_ERROR = 0,
    EXEC_INVALID_ARG,
    EXEC_PIPE_ERROR,
    EXEC_FORK_ERROR,
    EXEC_EXEC_ERROR,
    EXEC_UNKNOWN_ERROR
} exec_error_e;

typedef struct _exec_result_t {
  exec_error_e error = EXEC_NO_ERROR;
  string output;
} exec_result_t;

class FmwUtils
{
public:
    FmwUtils();
    ~FmwUtils();

	//Time
    static time_t getCurrentTimeSec();
    static std::string getCurrentDateTime(int type, std::string temp, bool isMilSec = false);
    static time_t convertUTCStrToTime(const std::string& str_utc, const char *format);

	//exec
	static exec_result_t execSync(const std::vector<std::string> &argv);
};

#endif // _FMWUTILS_H
