#include <memory>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <fcntl.h>
#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "ipc.h"
#include "FmwLogger.hpp"

extern volatile sig_atomic_t g_IsRunning;

ipc_server_t *server_init(request_handler_t req_handler, unsigned int port) {
    ipc_server_t *server = (ipc_server_t *)malloc(sizeof(ipc_server_t));
    if(server == NULL) {
        perror("malloc error");
        return NULL;
    }
    memset(server, 0, sizeof(ipc_server_t));

    /* Setup request handler */
    server->request_handler = req_handler;
    server->sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if(server->sockfd < 0) {
        perror("socket error");
        SAFE_FREE(server);
        return NULL;
    }

    struct sockaddr_in addr;
    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = inet_addr(SERVER_IP);
    addr.sin_port = htons(port);
    int optval = 1;

    setsockopt(server->sockfd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));

    if(bind(server->sockfd, (struct sockaddr *) &addr, sizeof(addr)) != 0) {
        perror("bind error");
        SAFE_CLOSE(server->sockfd);
        SAFE_FREE(server);
        return NULL;
    }

    if(listen(server->sockfd, BACKLOG) != 0) {
        perror("listen error");
        SAFE_CLOSE(server->sockfd);
        SAFE_FREE(server);
        return NULL;
    }

    return server;
}

static void *request_handle_routine(void *arg) {
    ipc_server_t *serverData = (ipc_server_t *)arg;

    ssize_t req_len;

    if(serverData == NULL) {
        ERROR_LOG("Error: invalid argument of thread routine\n");
        pthread_exit(0);
    }

    while (g_IsRunning) {
        auto req = make_shared<ipc_msg_t>();
        uint8_t *buf = (uint8_t *)(req.get());

        /* Receive request from client */
        req_len = recv_data(serverData->client_fd, (char *)buf, sizeof(ipc_msg_t), 0);

        if(req_len <= 0) {
            SAFE_CLOSE(serverData->client_fd);
            break;
        }

        serverData->request_handler(req);
    }

    pthread_exit(0);
}

int server_accept_request(ipc_server_t *server) {

    if(server == NULL) {
        ERROR_LOG("Error: invalid parameter!\n");
        return -1;
    }

    int client_fd = accept(server->sockfd, NULL, NULL);

    if(client_fd < 0) {
        DBG_LOG("accept errno return : %d\n", errno);
        return -1;
    }

    server->client_fd = client_fd;

    if(pthread_create(&server->thread_id, NULL, request_handle_routine, server) != 0) {
        SAFE_CLOSE(client_fd);
        return -1;
    }

    return 0;
}

ssize_t recv_data(int sockfd, char *buf, ssize_t len, int flags) {
    ssize_t bytes = recv(sockfd, buf, len, flags);
    if(bytes < 0) {
        bytes = 0;
    } else {
        if(bytes >= len) {
            INFO_LOG("The buffer is full\n");
        }
    }

    return bytes;
}

void server_close(ipc_server_t *server) {
    DBG_LOG("server_close()\n");

    if(server != NULL) {
        shutdown(server->sockfd, SHUT_RDWR);
        SAFE_CLOSE(server->sockfd);
        shutdown(server->client_fd, SHUT_RDWR);
        SAFE_CLOSE(server->client_fd);
        pthread_join(server->thread_id, NULL);
        SAFE_FREE(server);
    }
}

ipc_client_t *client_init(ipc_client_t *client, unsigned int port, int timeout) {
    struct sockaddr_in addr;
    int ret = 0;
    memset(client, 0, sizeof(ipc_client_t));

    memset(&addr, 0, sizeof(addr));

    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = inet_addr(SERVER_IP);
    addr.sin_port = htons(port);
    if(sockfd < 0) {
        return NULL;
    }
    client->sockfd = sockfd;

    do {
        ret = connect(client->sockfd, (struct sockaddr *)&addr, sizeof(addr));
        if(ret == 0) {
            break;
        } else {
            sleep(1);
        }
    } while (timeout-- > 0);

    if(ret != 0) {
        SAFE_CLOSE(client->sockfd);
        return NULL;
    }

    return client;
}

int client_send_request(ipc_client_t *client, char *buf, int len) {
    if(client == NULL) {
        ERROR_LOG("client_send_request() Error: invalid parameter!\n");
        return E_FAIL;
    }

    /* Send request */
    ssize_t bytes = send(client->sockfd, buf, len, MSG_NOSIGNAL);

    if(bytes != len) {
        ERROR_LOG("client_send_request() : send error return=%d, errno=%d \n", bytes, errno);
        return E_FAIL;
    }

    return E_SUCCESS;
}

 /**
 *        @brief Close the client socket fd and free memory.
 *        @details
 *        @param uds_client_t *c The pointer of client info
 *        @return None
 */
void client_close(ipc_client_t *client) {
    if(client != NULL) {
        SAFE_CLOSE(client->sockfd);
    }
}