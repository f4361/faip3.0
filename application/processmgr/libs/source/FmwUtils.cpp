/**
 * @file FmwTime.cpp
 * @author Kyunghoon Lim (kelvin.kh.lim@funzin.co.kr)
 * @brief 
 * @version 0.1
 * @date 2021-03-03
 * @details Null
 * @copyright Copyright (c) 2021
 * 
 */

#include "common.h"
#include "FmwUtils.h"
#include "FmwLogger.hpp"

FmwUtils::FmwUtils() {
}

FmwUtils::~FmwUtils() {
}

time_t FmwUtils::getCurrentTimeSec() {
    chrono::system_clock::time_point now_raw = chrono::system_clock::now();
    return chrono::system_clock::to_time_t(now_raw);
}

std::string FmwUtils::getCurrentDateTime(int type, std::string format, bool isMilSec) {
    chrono::system_clock::time_point now_raw = chrono::system_clock::now();
    time_t now = chrono::system_clock::to_time_t(now_raw);
    struct tm  tstruct = {0};
    char currTime[TIME_LEN] = {0,};
    
    if(type == LOCAL_TIME)
        localtime_r(&now, &tstruct);
    else if(type == UTC_TIME)
        gmtime_r(&now, &tstruct);  

    strftime(currTime, sizeof(currTime), format.c_str(), &tstruct);

    if(!isMilSec)
        return currTime;

    chrono::milliseconds milisec = chrono::duration_cast<chrono::milliseconds>(now_raw.time_since_epoch());

    stringstream retVal;
    retVal<<currTime<<":"<<setw(3)<<setfill('0')<<to_string(milisec.count()%1000);

    return retVal.str();
}

time_t FmwUtils::convertUTCStrToTime(const std::string& str_utc, const char *format) {
    struct tm time_struct = {0};
    std::string temp_str = str_utc;
    strptime(temp_str.c_str(), format, &time_struct);
    return timegm(&time_struct);
}

exec_result_t FmwUtils::execSync(const std::vector<std::string> &argv) {
    pid_t pid;
    int link[2];
    exec_result_t re;
    
    if (argv.size() == 0) {
        ERROR_LOG("Argument invalid Error!\n");
        re.error = EXEC_INVALID_ARG;
        return re;
    }
    
    if(pipe(link) == -1) {
        ERROR_LOG("Pipe Error!\n");
        re.error = EXEC_PIPE_ERROR;
        return re;
    }
    
    pid = fork(); 

    
    if(pid == 0) {
    
        if(close(link[0]) == -1) {
            ERROR_LOG("close(link[0]) error!!\n");
            exit(EXEC_PIPE_ERROR);
        }
        
        if(dup2(link[1], STDOUT_FILENO) == -1) {
             ERROR_LOG("Failed dup2\n");
             exit(EXEC_PIPE_ERROR);
        }
        
        if(close(link[1]) == -1) {
            ERROR_LOG("close(link[1]) error!!\n");
            exit(EXEC_PIPE_ERROR);
        }
        
        vector<char *> vector_argv;
        vector_argv.reserve(argv.size() + 1);
        for (auto s : argv) {
            vector_argv.push_back(strdup(s.c_str()));
        }
        vector_argv.push_back(NULL);
        
        if (execv(argv[0].c_str(), vector_argv.data()) == -1) {  
            ERROR_LOG("execv error!!\n");
            
            for (auto p : vector_argv) {
                free(p);
            }
        
            exit(EXEC_EXEC_ERROR);
        }    
    }else if(pid > 0) {
        int size;
        int status = 0;
        
        char data[128] = {0,};
        
        if(close(link[1]) == -1) {
            re.error = EXEC_PIPE_ERROR;
            return re;
        }
        
        size = read(link[0], data, 128);
        DBG_LOG("value read size = %d\n", size);
        re.output = data;
        
        pid_t pidChild = wait(&status);
		
        if(WIFEXITED(status)) {
            DBG_LOG("normal exit\n");
        } else {
            ERROR_LOG("abnormal exit status : %d %d\n", status, WEXITSTATUS(status));
        }
        re.error = (exec_error_e)WEXITSTATUS(status);

		if(pidChild == -1) {
			ERROR_LOG("fork wait error\n");
			re.error = EXEC_EXEC_ERROR;
        }
	
        if(close(link[0]) == -1) {
			ERROR_LOG("close(link[0]) error (parent side)\n");
            re.error = EXEC_PIPE_ERROR;
        }
    } else {
        re.error = EXEC_FORK_ERROR;
    }
    
    return re;
}

