/**
 * @file cloudMgr.hpp
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2022-04-01
 *
 * @copyright Copyright (c) 2022
 *
 */

#pragma once

#include <thread>
#include <mutex>
#include <map>
#include <list>
#include <iostream>
#include <cpprest/http_listener.h>
#include <cpprest/http_msg.h>
#include <cpprest/http_client.h>
#include <pplx/pplxtasks.h>
#include <sys/socket.h>


#include "FmwAppCore.hpp"
#include "ipc.h"

#define NORMAL_WAIT_TIME        10
#define CONNECT_CHECK_TIMEOUT   100
#define CLOUD_CONNECT_TIMEOUT  10000

#define REST_GET_SERVER_URL                            "http://0.0.0.0:18080"

typedef enum _cloud_connect_status_e {
    CLOUD_NOT_CONNECTED = 0x0000,
    CLOUD_CONNECTED     = 0x0001,
} cloud_connect_status_e;

enum {
    CLOUD_ERROR_NONE            = 0,
    CLOUD_ERROR_CONNECTION      = 1,
    CLOUD_ERROR_TIMEOUT         = 2,
    CLOUD_ERROR_INVALID_ARG     = 3,
    CLOUD_ERROR_RETRY           = 4,
    CLOUD_ERROR_UNKNOWN         = 5,
};

using namespace std;
using namespace web;
using namespace http;
using namespace web::http::experimental::listener;
using namespace web::http::client;

class cloudMgr : public Singleton<cloudMgr>
{
    friend class Singleton<cloudMgr>;

private:
    cloud_connect_status_e mStatus;
/*
    bool mIsRealTimeMonitoringOn;
    bool mIsSendingRealtimeMonitoring;
    real_time_monitoring_data_t mRealtimeMonitoringData;
*/
protected:
    cloudMgr();
    virtual ~cloudMgr();

public:    
    static void request_handler(shared_ptr<ipc_msg_t> msg);
    virtual void handle_request(shared_ptr<ipc_msg_t> msg);
    void initializeServer(request_handler_t req_handler);
    void sendToQt(json::value value);
    void sendAliveMessage();

    void startApplication(GMainLoop *pLoop) {
        m_threadPool = new FmwThread(20);
        m_pFmwAppCore.setGmainLoop(pLoop);
        cloudMgr::getInstance().initializeServer(request_handler);
        cloudMgr::getInstance().sendAliveMessage();
    }

    static json::value restHandle_post(char* data);   
    static gpointer serverThread(gpointer user_data); 
    static void reqQtCommand(shared_ptr<ipc_msg_t> msg, cloudMgr* pCloudMgr);
    void sendMessage(unsigned int to, char* buf, int len);

    int readBinData(char *filepath, char **data);
    void writeBinData(char* dstName, unsigned char* dstData, int len);

    json::value makeJsonForCloud(char* data);
    string makeJsonForQT(json::value srcJson);
    void convertToBase64File(shared_ptr<ipc_msg_t> msg);
    
    static void taskCloud();    
    int Cloud_Connect(cloud_connect_status_e& stat, int timeout_ms);
    int Cloud_Send(char* buf);
    int Cloud_Receive(cloud_connect_status_e& stat);
    void Cloud_socket_close(void);

public:
    int cloud_fd;
    string mUrl;
    int mPort;

    GThread *mThreadserverThread;
    ipc_server_t *m_pServer;
    map<unsigned int, ipc_client_t> m_clientsMap;
};
