#ifndef _BASE64_H_
#define _BASE64_H_
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <memory>

int base64_encode(char *text, int numBytes, char **encodedText);
int base64_decode(const char * text, unsigned char * dst, int numBytes);

#endif  // _BASE64_H_