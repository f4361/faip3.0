/**
 * @file common.h
 * @author kyunghoon Lim (kelvin.kh.lim@funzin.co.kr)
 * @brief
 * @version 0.1
 * @date 2021-03-02
 *
 * @copyright Copyright (c) 2020
 *
 */

#ifndef _COMMON_H
#define _COMMON_H
#include <glib.h>
#include <string>
#include <map>

using namespace std;

#define FILE_PATH_LEN   1024
#define E_SUCCESS       0
#define E_FAIL          -1

#define IMAGE_DIR_PATH  "/tmp/"

#define SAFE_CLOSE(X)   {if(X > 0) {close(X); X = 0; }}
#define SAFE_FREE(X)    {if (X) { free(X); X = NULL; }}
#define SAFE_DELETE(X)  {if (X) { delete(X); X = NULL;}}
#define SAFE_GTIMER_REMOVE(X)   {if(X) { g_source_remove(X); X = 0; }}


#endif  //_COMMON_H
