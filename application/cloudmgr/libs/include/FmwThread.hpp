/**
 * @file FmwThread.hpp
 * @author Kyunghoon Lim (kelvin.kh.lim@funzin.co.kr)
 * @brief 
 * @version 0.1
 * @date 2021-03-03
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#ifndef _FMWTHREAD_HPP
#define _FMWTHREAD_HPP
#include <chrono>
#include <condition_variable>
#include <cstdio>
#include <functional>
#include <future>
#include <mutex>
#include <queue>
#include <thread>
#include <vector>
#include "FmwLogger.hpp"

using namespace std;

extern volatile sig_atomic_t g_IsRunning;

class FmwThread {
public:
    FmwThread(size_t num_threads);
    ~FmwThread();

    template <class F, class... Args>
    void EnqueueJob(F&& f, Args&&... args);

private:
    size_t num_threads_;
    vector<thread> worker_threads_;
    queue<function<void()>> jobs_;
    condition_variable cv_job_q_;
    mutex m_job_q_;
    bool stop_all;
    void WorkerThread();
};

template <class F, class... Args>
void FmwThread::EnqueueJob(F&& f, Args&&... args) {
  if (!g_IsRunning) {
    ERROR_LOG("ThreadPool error\n");
    return;
  }

  auto job = make_shared<packaged_task<void()>>(bind(forward<F>(f), forward<Args>(args)...));
  {
    lock_guard<mutex> lock(m_job_q_);
    jobs_.push([job]() { (*job)(); });
  }
  cv_job_q_.notify_one();
}


#endif // _FMWTHREAD_HPP
