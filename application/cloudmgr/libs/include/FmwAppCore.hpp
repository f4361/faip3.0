/**
 * @file FmwAppCore.hpp
 * @author Kyunghoon Lim (kelvin.kh.lim@funzin.co.kr)
 * @brief
 * @version 0.1
 * @date 2021-03-03
 *
 * @copyright Copyright (c) 2021
 *
 */
#ifndef _FMWAPPCORE_HPP
#define _FMWAPPCORE_HPP

#include <glib.h>
#include <map>
#include <mutex>
#include "common.h"
#include "FmwThread.hpp"
#include "FmwLogger.hpp"

using namespace std;

class FmwAppCore {
public:
    FmwAppCore();
    virtual ~FmwAppCore();

    void regSignalHandler();
    void setGmainLoop(GMainLoop      *pGMainLoop) { m_pGMainLoop = pGMainLoop; }
    static gboolean quitMainLoop(gpointer user_data);

private:
    GMainLoop *m_pGMainLoop;
};

template <class T>
class Singleton {
public:
    static T &getInstance(void) {
        static T object;
        return object;
    }

    virtual void startApplication(GMainLoop *pLoop) = 0;

protected:
    Singleton() {}
    virtual ~Singleton() {}

    FmwAppCore m_pFmwAppCore;
    FmwThread *m_threadPool;

private:
    Singleton(const Singleton &) {}
    Singleton &operator=(const Singleton &) {}
};

#endif // _FMWAPPCORE_HPP


