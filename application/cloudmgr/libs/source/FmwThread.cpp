#include "FmwThread.hpp"
#include "FmwLogger.hpp"

void FmwThread::WorkerThread() {
    while (1) {
        std::unique_lock<std::mutex> lock(m_job_q_);
        cv_job_q_.wait(lock, [this]() { return !this->jobs_.empty() || !g_IsRunning; });
        if (!g_IsRunning && this->jobs_.empty()) {
            return;
        }

        std::function<void()> job = std::move(jobs_.front());
        jobs_.pop();
        lock.unlock();
        job();
    }
}

FmwThread::FmwThread(size_t num_threads)    : num_threads_(num_threads), stop_all(false) {
    worker_threads_.reserve(num_threads_);
    
    for (size_t i = 0; i < num_threads_; ++i) {
        worker_threads_.emplace_back([this]() { this->WorkerThread(); });
    }
}

FmwThread::~FmwThread() {
    DBG_LOG("FmwThread::FmwThread() start\n");
    stop_all = true;
    cv_job_q_.notify_all();
    DBG_LOG("FmwThread::FmwThread() #0\n");

    int i = 1;
    for (auto& t : worker_threads_) {
        DBG_LOG("FmwThread::FmwThread() #%d\n", i);
        t.join();
        i++;
    }
    DBG_LOG("FmwThread::FmwThread() end\n");
}

