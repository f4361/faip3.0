#include <glib-unix.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <dirent.h>
#include "FmwAppCore.hpp"
#include "FmwLogger.hpp"

volatile sig_atomic_t g_IsRunning = 1;

FmwAppCore::FmwAppCore() : m_pGMainLoop(NULL) {
    DBG_LOG("FmwAppCore::FmwAppCore()\n");
    regSignalHandler();
}

FmwAppCore::~FmwAppCore() {
    DBG_LOG("~FmwAppCore()\n");
}


gboolean FmwAppCore::quitMainLoop(gpointer user_data) {
    FmwAppCore *pFmwAppCore = reinterpret_cast<FmwAppCore *>(user_data);
    INFO_LOG("g_main_loop_quit()\n");
    g_IsRunning = 0;
    REMOVE_LOG_TIMER();
    g_main_loop_quit(pFmwAppCore->m_pGMainLoop);
    return FALSE;
}

void FmwAppCore::regSignalHandler()
{
    g_unix_signal_add (SIGINT, (GSourceFunc) quitMainLoop, this);
    g_unix_signal_add (SIGTERM, (GSourceFunc) quitMainLoop, this);
}

