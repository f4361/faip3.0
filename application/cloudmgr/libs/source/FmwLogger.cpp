/**
 * @file FmwLogger.cpp
 * @author Kyunghoon Lim (kelvin.kh.lim@funzin.co.kr)
 * @brief
 * @version 0.1
 * @date 2021-03-03
 * @details 로그용 버퍼사이즈를 제한합니다. 로그 버퍼를 많이 사용하는 경우 메시지 전송에 오류가 발생합니다.
 * @copyright Copyright (c) 2021
 *
 */
#include <sys/stat.h>
#include "common.h"
#include "FmwLogger.hpp"
#include "FmwUtils.h"

#define CHECKLOG_TIMEOUT    3
#define LOG_DIR             "logs"
#define LOG_FILE_SIZE       1024*1024*5
#define LOG_FILE_NUM_MAX    5
#define PATH_LEN            1024

FmwLogger::FmwLogger() : m_appLoglevel(0), m_appName("") {
    m_appNameMap["cloudmgr"]      = {"CM"};
}

FmwLogger::~FmwLogger() {
}

void FmwLogger::printLog(string level, const char * fmt, ...) {
    std::string log;
    std::ofstream m_file;

    log.append("[").append(FmwUtils::getCurrentDateTime(LOCAL_TIME,"%X",true)).append("]");
    log.append("[").append(level).append("]");
    log.append("[").append(m_appNameMap[m_appName]).append("]");

    va_list args, args_copy;

    va_start(args, fmt);
    va_copy(args_copy, args);

    std::string format;
    format.resize(vsnprintf(nullptr, 0, fmt, args));
    vsnprintf(format.data(), format.size()+1, fmt, args_copy);

    va_end(args_copy);
    va_end(args);

    log.append(" ").append(format);

    if(log.size() > LOG_STR_MAX)
        log.resize(LOG_STR_MAX);
#if 0
    m_file.open(m_logFullPath.c_str(), ios::app);
    m_file.write(log.c_str(), log.size());
    m_file.close();
#endif
    std::cerr<<log;
}

void FmwLogger::setAppName(string name) {
    m_appName = name;
    //setLogFile();
}

void FmwLogger::setLogLevel(uint8_t level) {
    m_appLoglevel = level;
}

uint8_t FmwLogger::getLogLevel() {
    //return m_appLoglevel;
    return 0;
}

void FmwLogger::setLogFile() {
    struct stat st;
    string logDir;

#ifndef DEF_FMW_DIR
    char logPath[1024];

    if(getcwd(logPath, 1024) == NULL) {
        ERROR_LOG("log :: getcwd error!!\n");
        return;
    }
    logDir.append(logPath).append("/").append(LOG_DIR);
#else
    if(stat(FMW_DIR_PATH, &st) < 0) {
        ERROR_LOG("/fwd directory does not exist!!!\n");
        mkdir(FMW_DIR_PATH, 0755);
    }

    logDir.append(FMW_DIR_PATH).append("/").append(LOG_DIR);
#endif
    if(stat(logDir.c_str(), &st) < 0) {
        INFO_LOG("Make %s directory\n", logDir.c_str());
        mkdir(logDir.c_str(), 0755);
    }

    m_logDirPath.append(logDir).append("/").append(m_appNameMap[m_appName]);
    if(stat(m_logDirPath.c_str(), &st) < 0) {
        INFO_LOG("Make %s directory\n", m_logDirPath.c_str());
        mkdir(m_logDirPath.c_str(), 0755);
    }

    m_logFileName.append(m_appNameMap[m_appName]).append("_").append("log.txt");
    m_logFullPath.append(m_logDirPath).append("/").append(m_logFileName);
    checkLogFile();
    m_checkLogTimerId = g_timeout_add_seconds(CHECKLOG_TIMEOUT, checkLogTimeoutCb, this);
}

void FmwLogger::checkLogFile() {
    if(!checkLogFileSize(m_logFullPath)) {
        renameLogFile();
    }
}

bool FmwLogger::checkLogFileSize(string strName) {
    struct stat sb;

    if (stat(strName.c_str(), &sb) == -1) {
       return TRUE;
    }

    if((long long) sb.st_size > LOG_FILE_SIZE)
        return FALSE;

    return TRUE;
}

bool FmwLogger::renameLogFile() {
    struct dirent *ent;
    int file_count = 0;
    string fileName;
    string oldName;
    string cmpName;
    char logList[LOG_FILE_NUM_MAX][PATH_LEN];
    DIR *dir;

    cmpName.append(m_appNameMap[m_appName]).append("_").append("log");
    dir = opendir(m_logDirPath.c_str());
    if(dir != NULL) {
        while ((ent = readdir (dir)) != NULL) {
            if(strncmp(ent->d_name, cmpName.c_str(), strlen(cmpName.c_str()))== 0) {
                strcpy(logList[file_count], ent->d_name);
                file_count++;
            }
        }
        closedir (dir);
    } else {
        return FALSE;
    }

    qsort(logList, file_count , sizeof(logList[0]), compare);
    if(file_count == LOG_FILE_NUM_MAX) {
        remove(logList[LOG_FILE_NUM_MAX-1]);
        file_count = file_count -1; // file_count = 4;
    }

    for(int i = file_count; i > 0; i--) {
        fileName.clear();
        oldName.clear();
        fileName.append(m_logDirPath).append("/").append(m_appNameMap[m_appName]).append("_").append("log").append(to_string(i)).append(".txt");
        oldName.append(m_logDirPath).append("/").append(logList[i-1]);
        rename(oldName.c_str(), fileName.c_str());
        printLog("I", "rename log file\n"); // Do not remove!! It has to the printlog after rename.
    }
    return TRUE;
}

int FmwLogger::compare(const void *a, const void *b) {
    return strcmp((char*)a, (char*)b);
}

gboolean FmwLogger::checkLogTimeoutCb(gpointer user_data) {
    FmwLogger* pFmwLogger = reinterpret_cast<FmwLogger *>(user_data);
    pFmwLogger->checkLogFile();
    return TRUE;
}

void FmwLogger::removeTimer() {
    INFO_LOG("remove log timer!!\n");
    SAFE_GTIMER_REMOVE(m_checkLogTimerId);
}

