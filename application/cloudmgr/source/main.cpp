#include <cstdio>
#include <unistd.h>
#include "FmwLogger.hpp"
#include "cloudMgr.hpp"

int main(int argc, char **argv)
{
    GMainLoop *pLoop = g_main_loop_new(NULL, FALSE);
    
    FmwLogger::getLogger().setAppName("cloudmgr");

    INFO_LOG("******************************** \n");
    INFO_LOG("Start CloudMgr\n");
    INFO_LOG("******************************** \n");

    cloudMgr::getInstance().startApplication(pLoop);

    g_main_loop_run(pLoop);

    //cloudMgr::getInstance().thread_cleanup();
}
