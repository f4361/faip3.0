#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <ifaddrs.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#include "cloudMgr.hpp"
#include "base64.h"

cloudMgr::cloudMgr() {
    m_clientsMap.clear();
}

cloudMgr::~cloudMgr() {
    server_close(m_pServer);
    g_thread_join(mThreadserverThread);
	SAFE_DELETE(m_threadPool);
}

json::value cloudMgr::restHandle_post(char* data) {  
    json::value vJson = json::value::null();
    json::value jsonObject = cloudMgr::getInstance().makeJsonForCloud(data);    
    string strJson = jsonObject.serialize();
    DBG_LOG("QT data = %s\n", data);
    
    #if 0
    vJson["status"] = json::value("OK");
    vJson["cameraNo"] = json::value(1);
    vJson["name"] = json::value("kelvin.kh");
    vJson["time"] = json::value("2022-05-16T18:43:58Z");
    vJson["face"] = jsonObject["picture"];
    //vJson["face"] = json::value("/tmp/12345g");
    #else
    http_client client(U("http://api.faip.co.kr/v1/device/detect"));
    http_request request(methods::POST);    

    request.headers().add(U("X-Auth-Id"), U("device"));
    request.headers().add(U("X-Auth-Token"), U("5403d443-b8b0-430e-a3c8-fa0f5f012897"));
    request.set_body(jsonObject);
    
    pplx::task<void> requestTask = client.request(request)                                 
    .then([](http_response response) {
        DBG_LOG("rest post response = %d\n", response.status_code());        
        return response.extract_json();
    })
    .then([&](pplx::task<web::json::value> previousTask) {
        try {
            vJson = previousTask.get();
        }
		catch (const web::http::http_exception& e) {
            ERROR_LOG("Error exception:%s\n", e.what());
        }
    });    

    try {
        requestTask.wait();
    }
    catch (const std::exception &e) {
        ERROR_LOG("Error exception:%s\n", e.what());
    }
    #endif

    string strStatus = vJson["status"].as_string();
    DBG_LOG("status = %s\n", strStatus.c_str());
    if(strStatus.compare("OK") != 0) {        
        return json::value::null();
    }
    
    return vJson;
}

void cloudMgr::initializeServer(request_handler_t req_handler) {
    m_pServer = server_init(req_handler, CLOUDMGR_PORT);
    if(NULL != m_pServer) {
        mThreadserverThread = g_thread_new("serverThread", serverThread, this);
    }
}

gpointer cloudMgr::serverThread(gpointer user_data) {
    while (g_IsRunning) {
        server_accept_request(cloudMgr::getInstance().m_pServer);
    }
    return NULL;
}

void cloudMgr::request_handler(shared_ptr<ipc_msg_t> msg) {
    shared_ptr<ipc_msg_t>req = msg;
    cloudMgr::getInstance().handle_request(req);    
}

void cloudMgr::handle_request(shared_ptr<ipc_msg_t> msg) {
    m_threadPool->EnqueueJob(reqQtCommand, msg, this);
}

void cloudMgr::sendMessage(unsigned int to, char* buf, int len) {    
    DBG_LOG("sendMessage : buf = %s, len = %d\n", buf, len);
    ipc_client_t client = {0,};
    if(m_clientsMap.find(to) != m_clientsMap.end()) {
        client = m_clientsMap[to];
    }

    if(client.sockfd == 0) {
        if(client_init(&client, to, 0) == NULL) {
            ERROR_LOG("Error - client init!\n");
            return;
        }
    }
    
    if(client_send_request(&client, buf, len) != E_SUCCESS) {
        client_close(&client);
        if(client_init(&client, to, 0) == NULL) {
            ERROR_LOG("Error - client init == 2\n");
            return;
        }
        client_send_request(&client, buf, len);
    }
    m_clientsMap.insert(pair<unsigned int, ipc_client_t>(to, client));
}

void cloudMgr::reqQtCommand(shared_ptr<ipc_msg_t> msg, cloudMgr* pCloudMgr) {           
    json::value vJson;  

    DBG_LOG("Receive from QT : path = %s\n", msg->data);  
    vJson = cloudMgr::getInstance().restHandle_post(msg->data);
    if(!vJson.is_null()) {
        cloudMgr::getInstance().sendToQt(vJson);
    } 
}

void cloudMgr::sendToQt(json::value value) {
    #if 1
    string result = makeJsonForQT(value);
    #else
    string result = value.serialize();
    #endif
    sendMessage(QT_PORT, (char*)result.c_str(), result.size());
}

void cloudMgr::sendAliveMessage() {
    json::value vJson;  
    string alive;

    vJson["appid"] = json::value("cloudmgr");
    vJson["status"] = json::value("alive");
    alive = vJson.serialize();
    sendMessage(PROCESSMGR_PORT, (char*)alive.c_str(), alive.size());
}

int cloudMgr::readBinData(char *filepath, char **data) {
    struct stat sb;
    int fd;
    int rd_size = 0;

    if (stat(filepath, &sb) == -1) {
       return 0;
    }

    DBG_LOG("Bin file size = %d\n", sb.st_size);
    (*data) = (char*)malloc(sb.st_size);

    if(0 < (fd = open(filepath, O_RDONLY))){
        rd_size = read(fd, *data, sb.st_size);
        DBG_LOG("read file size = %d\n", rd_size);
        close(fd);
    }
    else{
        ERROR_LOG("Bin file open failed!\n");
        free(data);
        return 0;
    }

    return rd_size;
}

void cloudMgr::writeBinData(char* dstName, unsigned char* dstData, int len) {
    int fd;

    DBG_LOG("write path = %s\n", dstName);
    if(0 < (fd = open(dstName, O_WRONLY | O_CREAT | O_EXCL, 0644))){
      write(fd, dstData, len);
      close(fd);
    }
    else{
        ERROR_LOG("write failed!!\n");
    }
}

json::value cloudMgr::makeJsonForCloud(char* data) {
    char* rdData = NULL;
    int dataSize = 0;   
    string strData = data;
    json::value jsonObject = json::value::parse(strData);
    json::value vJson;
    string path = jsonObject["picture"].as_string();    

    vJson["cameraNo"] = jsonObject["cameraNo"];
    vJson["format"] = jsonObject["format"];

    dataSize = cloudMgr::getInstance().readBinData((char*)path.c_str(), &rdData);
    if(dataSize != 0) {        
        vJson["picture"] = json::value(rdData);
        free(rdData);        
        DBG_LOG("data size = %d\n", dataSize);
    }

    if(remove((char*)path.c_str()) == 0) {
        DBG_LOG("%s file remove!!\n", (char*)path.c_str());
    }
    
    return vJson;
}

string cloudMgr::makeJsonForQT(json::value srcJson) {    
    json::value dstJson;
    unsigned char* decodeData = NULL;
    int decodedSize = 0;
    char resultPath[FILE_PATH_LEN];
    string face, time, result;

    face = srcJson["face"].as_string();
    time = srcJson["time"].as_string();
    dstJson["cameraNo"] = srcJson["cameraNo"];
    dstJson["name"] = srcJson["name"];
    dstJson["time"] = srcJson["time"];    
    DBG_LOG("face size = %d\n", face.size());

    decodeData = (unsigned char*)malloc(face.size());
    memset(decodeData, 0, face.size());
    decodedSize = base64_decode(face.c_str(), decodeData, face.size());
    DBG_LOG("decodedSize = %d\n", decodedSize);
    sprintf(resultPath, "%s%s", IMAGE_DIR_PATH, time.c_str());
    dstJson["face"] = json::value(resultPath);
    cloudMgr::getInstance().writeBinData(resultPath, decodeData, decodedSize);
    free(decodeData);
    result = dstJson.serialize();

    return result;
}

void cloudMgr::convertToBase64File(shared_ptr<ipc_msg_t> msg) {
    char* data = NULL;
    char* encodedData = NULL;
    int dataSize = 0, encodedSize = 0;
    char resultPath[FILE_PATH_LEN];

    json::value jsonObject = json::value::parse(msg->data);
    string path = jsonObject["picture"].as_string();    
    dataSize = cloudMgr::getInstance().readBinData((char*)path.c_str(), &data);
    if(dataSize != 0) {        
        encodedSize = base64_encode(data, dataSize, &encodedData);
        free(data);
        DBG_LOG("data size = %d\n", dataSize);
    }

    sprintf(resultPath, "%s%s_%s", IMAGE_DIR_PATH, "base64", "big_file");
    cloudMgr::getInstance().writeBinData(resultPath, (unsigned char*)encodedData, encodedSize);
    free(encodedData);
}

///////////////////////////////////////////////////////////////////
//                                                                //
//    Not used below logics!!!!!                                  //
//                                                                //
////////////////////////////////////////////////////////////////////

void cloudMgr::taskCloud() {
    cloud_connect_status_e stat = CLOUD_NOT_CONNECTED;
    if(getInstance().Cloud_Connect(stat, CLOUD_CONNECT_TIMEOUT) != CLOUD_ERROR_NONE){
        ERROR_LOG("Clound connect failed!!\n");
    }
    DBG_LOG("stat = %d\n", stat);

    while(g_IsRunning) {
        if(stat == CLOUD_CONNECTED) {
            if(getInstance().Cloud_Receive(stat) == CLOUD_ERROR_CONNECTION) {
                getInstance().Cloud_Connect(stat, CLOUD_CONNECT_TIMEOUT);
            }
        }
        else{
            getInstance().Cloud_Connect(stat, CLOUD_CONNECT_TIMEOUT);
        }
        usleep(NORMAL_WAIT_TIME*1000);
    }
}

int cloudMgr::Cloud_Connect(cloud_connect_status_e& stat, int timeout_ms) {
    struct sockaddr_in addr;
    struct hostent *host = NULL;
    long arg;
    int ret, err;
    struct timeval tv;
    fd_set fdset;
    socklen_t fdlen;

    stat = CLOUD_NOT_CONNECTED;

    if(timeout_ms < 1) {
       timeout_ms = 1;
    }

    if(cloud_fd >= 0) {
        close(cloud_fd);
        cloud_fd = -1;
    }

     if(cloud_fd < 0) {
        DBG_LOG("Cloud_Connect socket error(%d):%s\n", errno, strerror(errno));
        return CLOUD_ERROR_UNKNOWN;
    }

    memset(&addr, 0x00, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(mPort);
    if(inet_pton(AF_INET, mUrl.c_str(), &addr.sin_addr.s_addr) != 1) {
        DBG_LOG("Transform Cloud URL to IP Address\n");
        if((host = gethostbyname(mUrl.c_str())) == NULL) {
            DBG_LOG("Invalid Q.Cloud URL\n");
            return CLOUD_ERROR_UNKNOWN;
        }
        memcpy(&addr.sin_addr.s_addr, host->h_addr_list[0], host->h_length);
    }
    DBG_LOG("CLoud Server URL(Address):%s(%s) Port:%d\n", mUrl.c_str(), inet_ntoa(addr.sin_addr), mPort);

    //set nonblock socket
    if((arg = fcntl(cloud_fd, F_GETFL, NULL)) < 0) {
        DBG_LOG("F_GETFL error(%d):%s\n", errno, strerror(errno));
        Cloud_socket_close();
        return CLOUD_ERROR_UNKNOWN;
    }
    arg |= O_NONBLOCK;
    if(fcntl(cloud_fd, F_SETFL, arg) < 0) {
        DBG_LOG("F_SETFL error(%d):%s\n", errno, strerror(errno));
        Cloud_socket_close();
        return CLOUD_ERROR_UNKNOWN;
    }

    ret = connect(cloud_fd, (struct sockaddr*)&addr, sizeof(struct sockaddr));
    if(ret < 0) {
        if(errno == EINPROGRESS) {
            DBG_LOG("Connecting...\n");
            do {
                tv.tv_sec = timeout_ms/1000;
                tv.tv_usec = (timeout_ms%1000)*1000;
                FD_ZERO(&fdset);
                FD_SET(cloud_fd, &fdset);
                ret = select(cloud_fd+1, NULL, &fdset, NULL, &tv);
                if(ret < 0 && errno != EINTR) {
                    DBG_LOG("Cloud_Connect select error(%d):%s\n", errno, strerror(errno));
                    Cloud_socket_close();
                    return CLOUD_ERROR_UNKNOWN;
                } else if(ret > 0) {
                    fdlen = sizeof(err);
                    if(getsockopt(cloud_fd, SOL_SOCKET, SO_ERROR, (void*)(&err), &fdlen) < 0) {
                        DBG_LOG("Cloud_Connect getsockopt error(%d):%s\n", errno, strerror(errno));
                        Cloud_socket_close();
                        return CLOUD_ERROR_UNKNOWN;
                    }
                    if(err) {
                        DBG_LOG("Cloud_Connect getsockopt error(%d):%s\n", err, strerror(err));
                        Cloud_socket_close();
                        return CLOUD_ERROR_CONNECTION;
                    }
                    break;
                } else {
                    DBG_LOG("Cloud_Connect connection Timeout\n");
                    Cloud_socket_close();
                    return CLOUD_ERROR_CONNECTION;
                }
            } while(g_IsRunning);
            if(g_IsRunning == 0) {
                Cloud_socket_close();
                return CLOUD_ERROR_UNKNOWN;
            }
        } else {
            DBG_LOG("Cloud_Connect connection error(%d):%s\n", errno, strerror(errno));
            Cloud_socket_close();
            return CLOUD_ERROR_CONNECTION;
        }
    }
    DBG_LOG("Cloud connected!!\n");
    stat = CLOUD_CONNECTED;
    return CLOUD_ERROR_NONE;
}

int cloudMgr::Cloud_Send(char* buf) {
    int ret = 0, len = 0;

    if(buf == NULL)
        return -1;

    len = strlen(buf);
    if((ret = send(cloud_fd, buf, len, 0)) < 0) {
        ERROR_LOG("send failed! - errno = %s\n", strerror(errno));
        return -1;
    }

    DBG_LOG("Send success : buf = %s, ret = %d\n", buf, ret);
    return 0;
}

int cloudMgr::Cloud_Receive(cloud_connect_status_e& stat) {
    int ret = 0;
    char buf[FILE_PATH_LEN];

    if((ret = recv(cloud_fd, buf, sizeof(buf), 0)) < 0) {
        ERROR_LOG("recv failed! - errno = %s\n", strerror(errno));
        return -1;
    }
    else if(ret == 0){
        ERROR_LOG("Cloud_Receive - CLOUD_NOT_CONNECTED\n");
        stat = CLOUD_NOT_CONNECTED;
        return -1;
    }
    DBG_LOG("recv - %s\n", buf);
    // To do. process with buf.

    return 0;
}

void cloudMgr::Cloud_socket_close(void)
{
    close(cloud_fd);
    cloud_fd = -1;
}
